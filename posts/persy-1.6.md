---
title: Persy 1.6
published_date: 2024-12-30 15:41:11 +0000
layout: post.liquid
categories: ["news"]
---

Has been a few months from the last post, and more or less 10 months from the last minor,
I did not have much to include in a new minor, so things went a bit longer than usual, most of the work in the last months/year was on stability, 
I wrote a suite of crash tests, and run it for discovering issues, and verify the related fixes, this resulted in 3 hotfixes that solved most of the problem I discovered even though I'm still debugging some rare durability issues, 
in the meantime I did write a one new small feature so here we are with a new minor:

## Changes

This release add simple new feature, that allow to create a snapshot from a commit of a transaction, 
with a new commit method that return a snapshot at the exact state of the committed transaction, this was not possible before, 
because in case of concurrent commits it was impossible to control the transactions included in a snapshot in a deterministic way.

The specific method is called `commit_to_snapshot`  and is on the TransactionFinalize struct, here the [specific docs](https://docs.rs/persy/latest/persy/struct.TransactionFinalize.html#method.commit_to_snapshot).

In terms of features is all for this release, there have been refactors to easy out the maintenance, and there are some project and branches that I use for debugging and troubleshooting that are not part of this release
 and may not ever be part of any release, but are the biggest part of the work, obviously in this release are included all the fixes of the previous releases.

## Future

The work is still around stabilization, there are few rare problems I need to fix, and more crash tests need to be written, but I do feel I'm getting close to a stable enough version, so hopefully soon I can start work again 
on features and performance improvement.


### Support

If you want to support my work on Persy feel free to [sponsor me](https://liberapay.com/Persy/)

