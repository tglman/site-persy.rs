---
layout: post.liquid

title: Persy 0.3
published_date: 2018-03-04 21:00:30 -0500
categories: ["news"]
---

The third feature build up of a transactional storage is done.

## What can be done with Persy 0.3

- Everything that could be done with [0.2](/posts/persy-0.2.html)
- If a storage file is already open it cannot be open twice guaranteeing safety
- In case of crash the recover/rollback of a transaction can be chosen by a API
- A big set of other fixes

### Going on but not yet there

With the 0.3 and the ability to lock the file Persy start to have a basic usability, in any case for a storage
to provide a basic functionality it need to be at least integrated with some index capability 
to allow search of data reach decent performance, next minor will be focused on this.

In the internal there is a lot of work to do to improve cacheing,disc space reuse etc, many of this feature will improved after the 1.0 though 



