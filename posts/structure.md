---
layout: post.liquid

title: Internal Structure
published_date: 2018-03-05 21:00:30 -0500
categories: ["news"]
---

Persy use a single file to keep all the meta information and data, this file is split in pages of different size and different role.

### Page Size
The page size is variable but it follow the simple rule that a page can be only of the size of power of 2 (4,8,16,32 etc)

### Page Roles
There are few different roles that a page can be used for:
- **Meta configuration** there are a few pages that keep all the information of the configuration, segments, freelist, journal status 
- **Address** this are pages that keep the real position of a content given a id,this is used as well for update with copy on write 
- **Content** Pages that keep the actual content.
- **Journal** Pages that keep the log of the operations happened on the storage

  
A page does not have a constant role, if a page is freed can be reused in a completely different role.

### Page Organization

The file start with a root page that keeps some meta information and the pointers to the 3 main other structures: Journal, Address, Free Memory, each of this structure
has it's own root page with it's own meta informations, going on the detail of each structure:  

#### Journal
The journal has a root page that link to the first and the last page in the journal, the pages link each other as a link list, each new transaction is written at the end, allocation and linking new page as needed.
When all the transaction present in a page are committed successfully the page is going to be freed for reuse.  

#### Address
The address structure is split in segments, each segment has a root page that point to a link list of pages, each of this pages has inside some metadata of the record and the pointer to the page where the data is kept, the id of a record is the absolute position of the addresses pointer in the file.   
If all the record pointed by an address page are deleted the address page will be freed for reuse.

#### Free Memory
This is a simple array that keep N linked list of free pages available for reuse, one for each power of 2.  
  
  

  
Here is a diagram that try to show the storage structure:

![alt text](/images/structure-diagram.png "Structure Diagram")



