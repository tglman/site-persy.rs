---
layout: post.liquid

title: Getting Started
published_date: 2017-09-12 21:00:30 -0500
---

Persy is a storage based on a single file where data can stored in sub-containers called "segment", so as first step let's create a file the basic file add on it a segment and insert on it some raw data.

```rust

    //Create the file
    Persy::create("./storage.persy")?;
    //Open the file for operations
    let persy = Persy::open("./storage.persy",Config::new())?;
    //Start a transaction all the operations in persy are done inside a transaction.
    let mut tx = persy.begin()?;
    //Create a segment called "seg" using the started tx.
    persy.create_segment(&mut tx, "seg")?;
    //Prepere some raw data
    let data = vec![1;20];
    //Insert the data inside the segment with the current tx.
    let id = persy.insert_record(&mut tx, "seg", &data)?;
    //Commit the tx.
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

To notice that the transaction is a two phase transaction to allow to implement complex use cases.

Next step read scan the data that we just insert this can be done without a transaction

```rust 

   let id = None;
   let to_find = vec![1;20];
   for (read_id,content) in persy.scan("seg")? {
        //.... do something with the record.id and record.content
        if content == to_find {
            id = Some(read_id);
            break;
        }
    }

```

Update the record to another content
```rust 

    let update_id = //.... get from insert or from a scan.
    //Begin a transaction
    let mut tx = persy.begin()?;
    let new_data = vec![2;20];
    // Update the record with new data
    persy.update_record(&mut tx, "seg", &update_id, &new_data)?;
    //Commit the tx.
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

finally delete the record
```rust

    let delete_id = //.... get from insert or from a scan.
    //Begin a transaction
    let mut tx = persy.begin()?;
    // delete the record
    persy.delete_record(&mut tx, "seg", &delete_id)?;
    //Commit the tx.
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

```

Since 0.4 Persy includes some basic support of indexes, here a basic example on how to use and index together to a record.

``` rust 

    let mut tx = persy.begin()?;
    persy.create_segment(&mut tx, "data")?;
    persy.create_index::<String, PersyId>(&mut tx, "index", ValueMode::REPLACE)?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

    let mut tx = persy.begin()?;
    let rec = "record content".as_bytes();
    let id = persy.insert_record(&mut tx, "data", rec)?;

    persy.put::<String, PersyId>(&mut tx, "index", "key".to_string(), id)?;
    let prepared = persy.prepare_commit(tx)?;
    persy.commit(prepared)?;

    let read_id = persy.get::<String, PersyId>("index", &"key".to_string())?;
    if let Some(is_there) = read_id {
        if let Value::SINGLE(id) = is_there {
            let value = persy.read_record("data", &id)?;
            assert_eq!(Some(rec.to_vec()), value);
        }
    }
```

For a full overview of the capabilities of Persy, check the [docs](https://docs.rs/persy/0.5.0/persy/struct.Persy.html)



