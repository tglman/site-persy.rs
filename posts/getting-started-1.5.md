---
layout: post.liquid

title: Getting Started
published_date: 2019-11-25 21:00:30 -0500
---

Persy is a storage based on a single file where data can stored in sub-containers called "segment", so as first step let's create a file the basic file add on it a segment and insert on it some raw data.

```rust

    //Create the file
    Persy::create("./storage.persy")?;
    //Open the file for operations
    let persy = Persy::open("./storage.persy",Config::new())?;
    //Start a transaction all the operations in persy are done inside a transaction.
    let mut tx = persy.begin()?;
    //Create a segment called "seg" using the started tx.
    tx.create_segment("seg")?;
    //Prepere some raw data
    let data = vec![1;20];
    //Insert the data inside the segment with the current tx.
    let id = tx.insert("seg", &data)?;
    //Commit the tx.
    let prepared = tx.prepare()?;
    prepared.commit()?;

```

To notice that the transaction is a two phase transaction to allow to implement complex use cases.

Next step read scan the data that we just insert this can be done without a transaction

```rust 

   let id = None;
   let to_find = vec![1;20];
   for (read_id,content) in persy.scan("seg")? {
        //.... do something with the record.id and record.content
        if content == to_find {
            id = Some(read_id);
            break;
        }
    }

```

Update the record to another content
```rust 

    let update_id = //.... get from insert or from a scan.
    //Begin a transaction
    let mut tx = persy.begin()?;
    let new_data = vec![2;20];
    // Update the record with new data
    tx.update("seg", &update_id, &new_data)?;
    //Commit the tx.
    let prepared = tx.prepare()?;
    prepared.commit()?;

```

finally delete the record
```rust

    let delete_id = //.... get from insert or from a scan.
    //Begin a transaction
    let mut tx = persy.begin()?;
    // delete the record
    tx.delete_record("seg", &delete_id)?;
    //Commit the tx.
    let prepared = tx.prepare()?;
    prepared.commit()?;

```

Since 0.4 Persy includes some basic support of indexes, here a basic example on how to use and index together to a record.

``` rust 

    let mut tx = persy.begin()?;
    tx.create_segment("data")?;
    tx.create_index::<String, PersyId>("index", ValueMode::Replace)?;
    let prepared = tx.prepare()?;
    prepared.commit()?;

    let mut tx = persy.begin()?;
    let rec = "record content".as_bytes();
    let id = tx.insert("data", rec)?;

    tx.put::<String, PersyId>("index", "key".to_string(), id)?;
    let prepared = tx.prepare()?;
    prepared.commit()?;

    let read_id = persy.get::<String, PersyId>("index", &"key".to_string())?;
    if let Some(id) = read_id.next() {
        let value = persy.read("data", &id)?;
        assert_eq!(Some(rec.to_vec()), value);
    }
```

For a full overview of the capabilities of Persy, check the [docs](https://docs.rs/persy/1.5.0/persy/struct.Persy.html)



