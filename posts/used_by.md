---
layout: post.liquid

title: Used By
published_date: 2019-08-24 12:00:30 +0100
---
  
## Active 

#### opendal
 [source](https://github.com/apache/opendal) [crates-io](https://crates.io/crates/opendal)

#### librashader-cache
 [source](https://github.com/SnowflakePowered/librashader) [crates-io](https://crates.io/crates/librashader-cache)

#### rotools 
 [source](https://github.com/ronanyeah/rotools) [crates-io](https://crates.io/crates/rotools)

#### structsy
 [source](https://gitlab.com/tglman/structsy) [crates.io](https://crates.io/crates/structsy)
  
## Deprecated

#### typedb
 [source](https://github.com/mount-tech/typedb) [crates.io](https://crates.io/crates/typedb)

#### perkv
 [crates.io](https://crates.io/crates/perkv)

#### persawkv
 [crates.io](https://crates.io/crates/persawkv)
