---
layout: post.liquid

title: Persy 0.4
published_date: 2019-02-01 21:00:30 -0500
categories: ["news"]
---

Finally after long time the release 0.4 can be published, it's the biggest releases so far from the git stats we have: `27 files changed, 3620 insertions(+), 806 deletions(-)` and include most of the functionality I expected to 
be covered by this project, so the API of this release will be more or less the API of the 1.0.0 at least in terms of functionality, is expected some minor adjustment and some minor new API to be add.

## What can be done with Persy 0.4

- Everything that could be done with [0.3](/posts/persy-0.3.html)
- New Index Support 
  - Support of indexing now you can create an index on top of simple values (u8,u16,u32,u64,i8,i16,i32,i64,String,PersyId)
  - Customizable value management with 3 options: Exclusive, Cluster, Replace that cover different use cases:
	- Exclusive means that for a key just one value is allowed, in case of new put it will return a duplicate error.
	- Cluster means that multiple value are allowed for a key, though the single value cannot be duplicate
	- Replace means that only one value is allowed by a key, in case of put of a second value the new will replace the existing value
  - Integration in the transaction
- Some durability fixes that were also included in previous hotfixes 
- A big set of minor optimizations and cleanups coming from clippy suggestions
- Add of file sync on commit of the transaction
- Add of must use for transactions prepared commit, with the automatic rollback in case of prepare commit structure drop.

### Getting closer but not all is done

With 0.4 we have one of the biggest feature in, though is not yet great, is still missing some support for parallel operations on index now the implementation is based on a read write lock on the index, meaning that you can read in parallel but in case of write only one thread can be accessing the index,
Some of the problems of previous releases are still there like not optimal disk space use and caching algorithm, also the index API now support just put and get and at some point would be cool to have also iteration on index and ranges lookup.
anyway now that one of the most complex part is implemented this minor issues an be addressed soon.

