---
layout: post.liquid

title: Old Releases
published_date: 2018-03-04 21:00:30 -0500
---


## 1.5

[Release 1.5 ](/posts/persy-1.5.html)  
[ Getting Started 1.5 ](/posts/getting-started-1.5.html)  

## 1.4

[Release 1.4 ](/posts/persy-1.4.html)  
[ Getting Started 1.4 ](/posts/getting-started-1.4.html)  
 
## 1.3

[Release 1.3 ](/posts/persy-1.3.html)  
[ Getting Started 1.3 ](/posts/getting-started-1.3.html)  
 
## 1.2

[Release 1.2 ](/posts/persy-1.2.html)  
[ Getting Started 1.2 ](/posts/getting-started-1.2.html)  
 
## 1.1

[Release 1.1 ](/posts/persy-1.1.html)  
[ Getting Started 1.1 ](/posts/getting-started-1.1.html)  
 
## 1.0

[Release 1.0 ](/posts/persy-1.0.html)  
[ Getting Started 1.0 ](/posts/getting-started-1.0.html)  
 
## 0.11

[Release 0.11 ](/posts/persy-0.11.html)  
[ Getting Started 0.11 ](/posts/getting-started-0.11.html)  
 
## 0.10

[Release 0.10 ](/posts/persy-0.10.html)  
[ Getting Started 0.10 ](/posts/getting-started-0.10.html)  

## 0.9

[Release 0.9 ](/posts/persy-0.9.html)  
[ Getting Started 0.9 ](/posts/getting-started-0.9.html)  

## 0.8

[Release 0.8 ](/posts/persy-0.8.html)  
[ Getting Started 0.8 ](/posts/getting-started-0.8.html)  

## 0.7

[Release 0.7 ](/posts/persy-0.7.html)  
[ Getting Started 0.7 ](/posts/getting-started-0.7.html)  

## 0.6

[Release 0.6 ](/posts/persy-0.6.html)  
[ Getting Started 0.6 ](/posts/getting-started-0.6.html)  

## 0.5

[Release 0.5 ](/posts/persy-0.5.html)  
[ Getting Started 0.5 ](/posts/getting-started-0.5.html)  

## 0.4

[Release 0.4 ](/posts/persy-0.4.html)  
[ Getting Started 0.4 ](/posts/getting-started-0.4.html)  

## 0.3

[Release 0.3 ](/posts/persy-0.3.html)  
[ Getting Started 0.3 ](/posts/getting-started-0.3.html)  


## 0.2

[Release 0.2 ](/posts/persy-0.2.html)  
[ Getting Started 0.2 ](/posts/getting-started-0.2.html)  


## 0.1

[Release 0.1 ](/posts/persy-0.1.html)  
[ Getting Started 0.1 ](/posts/getting-started-0.1.html)  

